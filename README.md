# git2bunny

That role uploads files from a git repo to the BunnyCDN storage

## prerequisites

git on the control server

vars:

```yaml
git2bunny_enabled: true
git2bunny_repo: "https://git.repo/url.git"
git2bunny_zone: "storage-zone-name"
git2bunny_accesskey: "storage-access-key"
```

> **NOTE**: check [defaults/main.yml](./defaults/main.yml) to see full list of config options
